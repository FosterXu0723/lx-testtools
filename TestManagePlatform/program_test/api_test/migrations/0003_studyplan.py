# Generated by Django 2.1.7 on 2019-06-06 06:58

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api_test', '0002_user'),
    ]

    operations = [
        migrations.CreateModel(
            name='StudyPlan',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('plan_name', models.CharField(max_length=100)),
                ('learner_name', models.CharField(max_length=100)),
                ('contents', models.CharField(max_length=800)),
                ('status', models.CharField(max_length=30)),
                ('start_time', models.CharField(max_length=30)),
                ('end_time', models.CharField(max_length=30)),
                ('plan_scheduled', models.CharField(max_length=30)),
                ('create_time', models.DateTimeField(auto_now=True)),
            ],
        ),
    ]
