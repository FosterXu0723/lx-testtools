from django.db import models

# Create your models here.

class TestPlan(models.Model):
    plan_name=models.CharField(max_length=100)
    req_number=models.CharField(max_length=100)
    tester_name=models.CharField(max_length=100)
    status=models.CharField(max_length=30)
    start_time = models.CharField(max_length=30)
    end_time = models.CharField(max_length=30)
    create_time = models.DateTimeField(auto_now=True)

class User(models.Model):
    username=models.CharField(max_length=100)
    password=models.CharField(max_length=100)
    phone=models.CharField(max_length=60)
    mail=models.CharField(max_length=100)
    is_staff=models.IntegerField()
    create_time=models.DateTimeField(auto_now=True)

class StudyPlan(models.Model):
    plan_name=models.CharField(max_length=100)
    learner_name=models.CharField(max_length=100)
    contents=models.CharField(max_length=800)
    status=models.CharField(max_length=30)
    start_time = models.CharField(max_length=30)
    end_time = models.CharField(max_length=30)
    plan_scheduled=models.CharField(max_length=30)
    create_time = models.DateTimeField(auto_now=True)