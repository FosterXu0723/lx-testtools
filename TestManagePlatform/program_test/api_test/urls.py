from django.conf.urls import url
from api_test import views_TestPlanManageApi
from api_test import views_StudyPlanManageApi
from api_test import views_SystemApi
app_name='api_test'
urlpatterns=[
    #添加测试计划接口
    url('add_testplan/',views_TestPlanManageApi.add_testplan,name='add_testplan'),
    #编辑测试计划接口
    url('edit_testplan/',views_TestPlanManageApi.edit_testplan,name='edit_testplan'),
    #查看测试计划接口
    url('look_testplan/',views_TestPlanManageApi.look_testplan,name='look_testplan'),
    #删除测试计划接口
    url('del_testplan/',views_TestPlanManageApi.del_testplan,name='del_testplan'),
    #测试计划分页接口
    url('paging_testplan/',views_TestPlanManageApi.paging_testplan,name='paging_testplan'),
    # 登录接口
    url('login/', views_SystemApi.login,name='login'),
    #统计测试计划接口
    url('count_testplan/',views_TestPlanManageApi.count_testplan,name='count_testplan'),
    #查询测试计划接口
    url('query_testplan/',views_TestPlanManageApi.query_testplan,name='query_testplan'),
    #查询测试计划分页接口
    url('pagect_testplan/',views_TestPlanManageApi.pagect_testplan,name='pagect_testplan'),
    #添加学习计划接口
    url('add_studyplan/',views_StudyPlanManageApi.add_studyplan,name='add_studyplan'),
    #学习计划分页接口
    url('paging_studyplan/',views_StudyPlanManageApi.paging_studyplan,name='paging_studyplan'),
    #统计学习计划接口
    url('count_studyplan/',views_StudyPlanManageApi.count_studyplan,name='count_studyplan'),
    #查询学习计划接口
    url('query_studyplan/',views_StudyPlanManageApi.query_studyplan,name='query_studyplan'),
    #查询学习计划分页接口
    url('pagect_studyplan/',views_StudyPlanManageApi.pagect_studyplan,name='pagect_studyplan'),
    #删除学习计划接口
    url('del_studyplan/',views_StudyPlanManageApi.del_studyplan,name='del_studyplan')
]