#coding=utf-8
from django.http import JsonResponse
from api_test.models import StudyPlan
from datetime import datetime
from django.core.exceptions import ValidationError
import logging
import logging.handlers
from django.core.paginator import Paginator,EmptyPage,PageNotAnInteger
from django.http import HttpResponse
from django.core import serializers
import simplejson
import json
from django.db.models import Q
import pymysql

conn=pymysql.connect(host='192.168.1.191',user='root',password='123456',database='program_test')
cursor=conn.cursor()
logger = logging.getLogger('api_test.views_StudyPlanManageApi')

#学习计划分页接口
def paging_studyplan(request):
    logger.debug(123)
    if request.method=='GET':
        currentPage=request.GET.get('currentPage')
        #sql='select * from api_test_studyplan'
        #cursor.execute(sql)
        #tp_list=cursor.fetchall()
        #tp_list=StudyPlan.objects.all()
        paginator=Paginator(tp_list,10)
        logger.info(tp_list)
        logger.info(paginator)
        try:
            contacts=paginator.page(currentPage)
        except PageNotAnInteger:
            contacts = paginator.page(1)
        except EmptyPage:
            contacts=paginator.page(paginator.num_pages)
        logger.info(contacts)
        json_data=simplejson.dumps([{'plan_name':c.plan_name,'learner_name': c.learner_name,"status":c.status,"start_time":c.start_time,"end_time":c.end_time}for c in contacts])
        return HttpResponse(json_data,content_type='application/json;charset=utf-8')


#统计测试计划接口
def  count_studyplan(request):
    if request.method=='GET':
        total_plan=StudyPlan.objects.count()
        return JsonResponse({'status':200,'total':total_plan})
    else:
        return JsonResponse({'status':9999,'message':'请求方法错误'})

#查询学习计划接口
def query_studyplan(request):
    if request.method=='GET':
        currentPage=request.GET.get('currentPage')
        plan_name=request.GET.get('plan_name', '')
        learner_name=request.GET.get('learner_name', '')
        status = request.GET.get('status', '')
        if plan_name!="" and learner_name=="" and status=="":
            tp_list=StudyPlan.objects.filter(plan_name__contains=plan_name)
        if plan_name=="" and learner_name!="" and status=="":
            tp_list=StudyPlan.objects.filter(learner_name=learner_name)
        if plan_name=="" and learner_name=="" and status!="":
            tp_list=StudyPlan.objects.filter(status=status)
        if plan_name!="" and learner_name!="" and status=="":
            tp_list=StudyPlan.objects.filter(plan_name__contains=plan_name,learner_name=learner_name)
        if plan_name!="" and learner_name=="" and status!="":
            tp_list=StudyPlan.objects.filter(plan_name__contains=plan_name,status=status)
        if plan_name=="" and learner_name!="" and status!="":
            tp_list=StudyPlan.objects.filter(learner_name=learner_name,status=status)
        if plan_name!="" and learner_name!="" and status!="":
            tp_list=StudyPlan.objects.filter(plan_name__contains=plan_name,learner_name=learner_name,status=status)
        if plan_name=="" and learner_name=="" and status=="":
            tp_list=StudyPlan.objects.all()
        #tp_list=TestPlan.objects.filter(Q(plan_name__contains=plan_name)|Q(tester_name=tester_name)|Q(status=status))
        paginator=Paginator(tp_list,10)
        try:
            contacts=paginator.page(currentPage)
        except PageNotAnInteger:
            contacts = paginator.page(1)
        except EmptyPage:
            contacts=paginator.page(paginator.num_pages)
        json_data=simplejson.dumps([{'plan_name':c.plan_name,'learner_name': c.learner_name,"status":c.status,"start_time":c.start_time,"end_time":c.end_time}for c in contacts])
        return HttpResponse(json_data,content_type='application/json;charset=utf-8')
    else:
        return JsonResponse({'status':9999,'message':'请求方法错误'})

#查询后分页接口
def pagect_studyplan(request):
    if request.method=='GET':
        plan_name=request.GET.get('plan_name', '')
        learner_name=request.GET.get('learner_name', '')
        status = request.GET.get('status', '')
        if plan_name!="" and learner_name=="" and status=="":
            total_plan=StudyPlan.objects.filter(plan_name__contains=plan_name).count()
        if plan_name=="" and learner_name!="" and status=="":
            total_plan=StudyPlan.objects.filter(learner_name=learner_name).count()
        if plan_name=="" and learner_name=="" and status!="":
            total_plan=StudyPlan.objects.filter(status=status).count()
        if plan_name!="" and learner_name!="" and status=="":
            total_plan=StudyPlan.objects.filter(plan_name__contains=plan_name,learner_name=learner_name).count()
        if plan_name!="" and learner_name=="" and status!="":
            total_plan=StudyPlan.objects.filter(plan_name__contains=plan_name,status=status).count()
        if plan_name=="" and learner_name!="" and status!="":
            total_plan=StudyPlan.objects.filter(learner_name=learner_name,status=status).count()
        if plan_name!="" and learner_name!="" and status!="":
            total_plan=StudyPlan.objects.filter(plan_name__contains=plan_name,learner_name=learner_name,status=status).count()
        if plan_name=="" and learner_name=="" and status=="":
            total_plan=StudyPlan.objects.all().count()
        return JsonResponse({'status':200,'total':total_plan})
    else:
        return JsonResponse({'status':9999,'message':'请求方法错误'})



#添加学习计划接口
def add_studyplan(request):
    if request.method=='POST':
        plan_name=request.POST.get('plan_name','')
        learner_name=request.POST.get('learner_name','')
        status=request.POST.get('status','')
        start_time=request.POST.get('start_time','')
        end_time=request.POST.get('end_time','')
        plan_scheduled=request.POST.get('percentage','')
        contents=request.POST.get('contents','')
        pn_list=StudyPlan.objects.values_list('plan_name',flat=True)
        logger.debug(pn_list)
        if plan_name=='' or learner_name=='' or status=='':
            return JsonResponse({'status':9999,'message':'计划名称、学习人员、状态不能为空！'})
        if plan_name in pn_list:
            return JsonResponse({'status': 9999, 'message': '计划名称已存在！'})
        else:
            StudyPlan.objects.create(plan_name=plan_name,learner_name=learner_name,status=status,start_time=start_time,end_time=end_time,plan_scheduled=plan_scheduled,contents=contents)
            return JsonResponse({'status':200,'message':'添加学习计划成功'})
    else:
        return JsonResponse({'status':9999,'message':'请求方法错误'})

#删除学习计划接口
def del_studyplan(request):
    if request.method == 'POST':
        plan_name=request.POST.get('plan_name', '')
        if plan_name=='':
            return JsonResponse({'status': 9999, 'message':'计划名称不能为空！'})
        else:
            StudyPlan.objects.filter(plan_name=plan_name).delete()
            return JsonResponse({'status':200,'message':'删除学习计划成功！'})
    else:
        return JsonResponse({'status':9999,'message':'请求方法错误'})


#查看学习计划接口










