#coding=utf-8
from django.http import JsonResponse
from api_test.models import TestPlan,User
from datetime import datetime
from django.core.exceptions import ValidationError
import logging
import logging.handlers
from django.core.paginator import Paginator,EmptyPage,PageNotAnInteger
from django.http import HttpResponse
from django.core import serializers
import simplejson
import json
from django.db.models import Q


logger = logging.getLogger('api_test.views_TestPlanManageApi')
#添加测试计划接口
def add_testplan(request):
    if request.method=='POST':
        plan_name=request.POST.get('plan_name','')
        req_number=request.POST.get('req_number','')
        tester_name=request.POST.get('tester_name','')
        status=request.POST.get('status','')
        start_time=request.POST.get('start_time','')
        end_time=request.POST.get('end_time','')
        pn_list=TestPlan.objects.values_list('plan_name',flat=True)
        logger.debug(pn_list)
        if plan_name=='' or tester_name=='' or status=='':
            return JsonResponse({'status':9999,'message':'计划名称、测试人员、状态不能为空！'})
        if plan_name in pn_list:
            return JsonResponse({'status': 9999, 'message': '计划名称已存在！'})
        else:
            TestPlan.objects.create(plan_name=plan_name,req_number=req_number,tester_name=tester_name,status=status,start_time=start_time,end_time=end_time)
            return JsonResponse({'status':200,'message':'添加测试计划成功'})
    else:
        return JsonResponse({'status':9999,'message':'请求方法错误'})

#编辑测试计划接口
def edit_testplan(request):
    if request.method == 'POST':
        plan_name = request.POST.get('plan_name', '')
        req_number = request.POST.get('req_number', '')
        tester_name = request.POST.get('tester_name', '')
        status = request.POST.get('status', '')
        start_time = request.POST.get('start_time', '')
        end_time = request.POST.get('end_time', '')
        pn_list=TestPlan.objects.values_list('plan_name',flat=True)
        logger.debug(pn_list)
        if tester_name=='' or status=='':
            return JsonResponse({'status':9999,'message':'测试人员、状态不能为空！'})
        #if plan_name in pn_list:
            #return JsonResponse({'status': 9999, 'message': '计划名称已存在！'})
        else:
            TestPlan.objects.filter(plan_name=plan_name).update(req_number=req_number,tester_name=tester_name,status=status,start_time=start_time,end_time=end_time)
            return JsonResponse({'status':200, 'message':'修改测试计划成功'})
    else:
        return JsonResponse({'status':9999,'message':'请求方法错误'})

#查看测试计划接口
def look_testplan(request):
    if request.method=='GET':
        plan_name=request.GET.get('plan_name', '')
        if plan_name=='':
            return JsonResponse({'status': 9999, 'message':'计划名称不能为空！'})
        else:
            pn=TestPlan.objects.get(plan_name=plan_name)
            pn_list=[{'plan_name':pn.plan_name,'req_number':pn.req_number,'tester_name':pn.tester_name,'status':pn.status,'start_time':pn.start_time,'end_time':pn.end_time}]
            logger.debug(pn_list)
            return JsonResponse({'status':200,'message':pn_list})
    else:
        return JsonResponse({'status':9999, 'message':'请求方法错误'})

#删除测试计划接口
def del_testplan(request):
    if request.method == 'POST':
        b=request.body
        logger.debug(b)
        plan_name=request.POST.get('plan_name', '')
        if plan_name=='':
            return JsonResponse({'status': 9999, 'message':'计划名称不能为空！'})
        else:
            TestPlan.objects.filter(plan_name=plan_name).delete()
            return JsonResponse({'status':200,'message':'删除测试计划成功！'})
    else:
        return JsonResponse({'status':9999,'message':'请求方法错误'})


#测试计划分页接口
def paging_testplan(request):
    if request.method=='GET':
        currentPage=request.GET.get('currentPage')
        tp_list=TestPlan.objects.all()
        paginator=Paginator(tp_list,10)
        try:
            contacts=paginator.page(currentPage)
        except PageNotAnInteger:
            contacts = paginator.page(1)
        except EmptyPage:
            contacts=paginator.page(paginator.num_pages)
        json_data=simplejson.dumps([{'plan_name':c.plan_name,'tester_name': c.tester_name,"status":c.status,"start_time":c.start_time,"end_time":c.end_time}for c in contacts])
        #json_data1=(json_data,{"total":total_plan})
        #json_data=serializers.serialize("json",contacts,ensure_ascii=False)
        #contacts_list=list(contacts)
        #json_data=json.dumps(contacts_list)
        #for s1 in json_data:
            #logger.debug(s1)
        #logger.debug(contacts)
        #logger.debug(json_data)
        #return HttpResponse(json_data,charset='utf-8')
        return HttpResponse(json_data,content_type='application/json;charset=utf-8')
        
    else:
        return JsonResponse({'status':9999,'message':'请求方法错误'})
#统计测试计划接口
def  count_testplan(request):
    if request.method=='GET':
        total_plan=TestPlan.objects.count()
        return JsonResponse({'status':200,'total':total_plan})
    else:
        return JsonResponse({'status':9999,'message':'请求方法错误'})

#查询测试计划接口
def query_testplan(request):
    if request.method=='GET':
        currentPage=request.GET.get('currentPage')
        plan_name=request.GET.get('plan_name', '')
        tester_name=request.GET.get('tester_name', '')
        status = request.GET.get('status', '')
        if plan_name!="" and tester_name=="" and status=="":
            tp_list=TestPlan.objects.filter(plan_name__contains=plan_name)
            rr=tp_list.count()
            logger.debug(rr)
        if plan_name=="" and tester_name!="" and status=="":
            tp_list=TestPlan.objects.filter(tester_name=tester_name)
        if plan_name=="" and tester_name=="" and status!="":
            tp_list=TestPlan.objects.filter(status=status)
        if plan_name!="" and tester_name!="" and status=="":
            tp_list=TestPlan.objects.filter(plan_name__contains=plan_name,tester_name=tester_name)
        if plan_name!="" and tester_name=="" and status!="":
            tp_list=TestPlan.objects.filter(plan_name__contains=plan_name,status=status)
        if plan_name=="" and tester_name!="" and status!="":
            tp_list=TestPlan.objects.filter(tester_name=tester_name,status=status)
        if plan_name!="" and tester_name!="" and status!="":
            tp_list=TestPlan.objects.filter(plan_name__contains=plan_name,tester_name=tester_name,status=status)
        if plan_name=="" and tester_name=="" and status=="":
            tp_list=TestPlan.objects.all()
        #tp_list=TestPlan.objects.filter(Q(plan_name__contains=plan_name)|Q(tester_name=tester_name)|Q(status=status))
        paginator=Paginator(tp_list,10)
        try:
            contacts=paginator.page(currentPage)
        except PageNotAnInteger:
            contacts = paginator.page(1)
        except EmptyPage:
            contacts=paginator.page(paginator.num_pages)
        json_data=simplejson.dumps([{'plan_name':c.plan_name,'tester_name': c.tester_name,"status":c.status,"start_time":c.start_time,"end_time":c.end_time}for c in contacts])
        return HttpResponse(json_data,content_type='application/json;charset=utf-8')
    else:
        return JsonResponse({'status':9999,'message':'请求方法错误'})

#查询后分页接口
def pagect_testplan(request):
    if request.method=='GET':
        plan_name=request.GET.get('plan_name', '')
        tester_name=request.GET.get('tester_name', '')
        status = request.GET.get('status', '')
        if plan_name!="" and tester_name=="" and status=="":
            total_plan=TestPlan.objects.filter(plan_name__contains=plan_name).count()
        if plan_name=="" and tester_name!="" and status=="":
            total_plan=TestPlan.objects.filter(tester_name=tester_name).count()
        if plan_name=="" and tester_name=="" and status!="":
            total_plan=TestPlan.objects.filter(status=status).count()
        if plan_name!="" and tester_name!="" and status=="":
            total_plan=TestPlan.objects.filter(plan_name__contains=plan_name,tester_name=tester_name).count()
        if plan_name!="" and tester_name=="" and status!="":
            total_plan=TestPlan.objects.filter(plan_name__contains=plan_name,status=status).count()
        if plan_name=="" and tester_name!="" and status!="":
            total_plan=TestPlan.objects.filter(tester_name=tester_name,status=status).count()
        if plan_name!="" and tester_name!="" and status!="":
            total_plan=TestPlan.objects.filter(plan_name__contains=plan_name,tester_name=tester_name,status=status).count()
        if plan_name=="" and tester_name=="" and status=="":
            total_plan=TestPlan.objects.all().count()
        return JsonResponse({'status':200,'total':total_plan})
    else:
        return JsonResponse({'status':9999,'message':'请求方法错误'})









