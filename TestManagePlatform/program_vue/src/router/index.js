import Vue from 'vue'
import Router from 'vue-router'
import Login from '@/components/Login/Login'
import FirstPage from '@/components/FirstPage/FirstPage'
import TestPlanManage from '@/components/PlanManage/TestPlanManage'
import StudyPlanManage from '@/components/PlanManage/StudyPlanManage'
Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/Login',
      name: 'Login',
      component: Login
    },
    {
      path: '/',
      redirect: '/FirstPage'
    },
    {
      path: '/',
      component: () => import('@/components/commn/container'),
      children: [
        {
          path: '/FirstPage',
          name: 'FirstPage',
          component: FirstPage
        },
        {
          path: '/TestPlanManage',
          name: 'TestPlanManage',
          component: TestPlanManage
        },
        {
          path: '/StudyPlanManage',
          name: 'StudyPlanManage',
          component: StudyPlanManage
        }
      ]
    }
  ]
})
